# README #

A Spring boot app that generates A stock order advice for the given criteria.

To run the app,
command line
	./gradlew bootRun
	
	./gradlew clean build
	java -jar ./build/libs/Stock-Check-Service-1.0.0-SNAPSHOT.jar
	
IDE(Intellij)
	Run StockCheckApplication
	
To get stock order advice,
	 http://localhost:8080/stock/order/advice
	 (when application starts up in memory HSQL database is loaded with some intial data)

Uses
	Spring Data Jpa
	jeasy Mvel rules
	HsqlDB
	lombok
	
To run cucumber tests
	./gradlew acceptanceTest
	
