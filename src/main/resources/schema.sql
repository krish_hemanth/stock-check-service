CREATE SCHEMA RULESTORE;
CREATE TABLE RULESTORE.RULE(ID varchar(5) CONSTRAINT RULE_pk primary key, NAME VARCHAR(1024) not null, CONDITION VARCHAR(2048) not null, ACTION VARCHAR(1024) not null, RULE_ORDER int not null, DESCRIPTION VARCHAR(1024) not null, UNIQUE(NAME), UNIQUE(RULE_ORDER));

CREATE SCHEMA INVENTORY;
CREATE TABLE INVENTORY.STOCK_ITEM(ID varchar(5) CONSTRAINT STOCK_pk primary key, ITEM_NAME VARCHAR(255) not null, ITEM_DESCRIPTION VARCHAR(1024) not null, ITEM_STOCK int not null, UNIQUE(ITEM_NAME));

CREATE SCHEMA AUDIT;
CREATE TABLE AUDIT.AUDIT(ID INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) CONSTRAINT AUDIT_pk primary key, AUDIT_TYPE VARCHAR(25) not null, AUDIT_RECORD VARCHAR(2048) not null, AUDIT_DATETIME VARCHAR(2048) not null);