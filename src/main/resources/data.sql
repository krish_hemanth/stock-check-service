INSERT INTO RULESTORE.RULE(ID, NAME, CONDITION, ACTION, RULE_ORDER, DESCRIPTION) VALUES ('1', 'Minimum stock rule - A,B,C & E', '(((fact.itemName.equals("A")) || (fact.itemName.equals("B")) || (fact.itemName.equals("C")) || (fact.itemName.equals("E"))) && fact.stockLevel<=4)', 'fact.orderType=OrderType.REORDER', 1, 'Min stock rule - if current quantity less than 4 for A,B,C and E');
INSERT INTO RULESTORE.RULE(ID, NAME, CONDITION, ACTION, RULE_ORDER, DESCRIPTION) VALUES ('2', 'Minimum stock rule - D', '((fact.itemName.equals("D")) && fact.stockLevel<=8)', 'fact.orderType=OrderType.REORDER', 2, 'Min stock rule - if current quantity less than 8 for D');
INSERT INTO RULESTORE.RULE(ID, NAME, CONDITION, ACTION, RULE_ORDER, DESCRIPTION) VALUES ('3', 'Blocked - C', 'fact.itemName.equals("C")', 'fact.orderType=OrderType.BLOCKED', 3, 'item C is blocked for further reorder');
INSERT INTO RULESTORE.RULE(ID, NAME, CONDITION, ACTION, RULE_ORDER, DESCRIPTION) VALUES ('4', 'One off order - D', 'fact.itemName.equals("D")', 'fact.oneOffQuantity=15', 4, 'item D has one off order of 15');


INSERT INTO INVENTORY.STOCK_ITEM(ID, ITEM_NAME, ITEM_DESCRIPTION, ITEM_STOCK) VALUES ('1', 'A', 'Item A', 5);
INSERT INTO INVENTORY.STOCK_ITEM(ID, ITEM_NAME, ITEM_DESCRIPTION, ITEM_STOCK) VALUES ('2', 'B', 'Item B', 8);
INSERT INTO INVENTORY.STOCK_ITEM(ID, ITEM_NAME, ITEM_DESCRIPTION, ITEM_STOCK) VALUES ('3', 'C', 'Item C', 2);
INSERT INTO INVENTORY.STOCK_ITEM(ID, ITEM_NAME, ITEM_DESCRIPTION, ITEM_STOCK) VALUES ('5', 'E', 'Item E', 1);
INSERT INTO INVENTORY.STOCK_ITEM(ID, ITEM_NAME, ITEM_DESCRIPTION, ITEM_STOCK) VALUES ('4', 'D', 'Item D', 0);

