package com.comcarde.stockcheck.database.repository;

import com.comcarde.stockcheck.database.entity.AuditRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditRepository extends CrudRepository<AuditRecord, String> {
}
