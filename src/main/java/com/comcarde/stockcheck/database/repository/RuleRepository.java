package com.comcarde.stockcheck.database.repository;

import com.comcarde.stockcheck.database.entity.Rule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends CrudRepository<Rule, String> {
}