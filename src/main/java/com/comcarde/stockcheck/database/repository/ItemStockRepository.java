package com.comcarde.stockcheck.database.repository;

import com.comcarde.stockcheck.database.entity.Rule;
import com.comcarde.stockcheck.database.entity.StockItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemStockRepository extends CrudRepository<StockItem, String> {
}
