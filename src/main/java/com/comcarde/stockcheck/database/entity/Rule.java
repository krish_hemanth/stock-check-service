package com.comcarde.stockcheck.database.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RULE", schema = "RULESTORE")
public class Rule implements Serializable {
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private String ruleId;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "CONDITION", nullable = false)
    private String condition;

    @Column(name = "ACTION", nullable = false)
    private String action;

    @Column(name = "RULE_ORDER", unique = true, nullable = false)
    private Integer order;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

}
