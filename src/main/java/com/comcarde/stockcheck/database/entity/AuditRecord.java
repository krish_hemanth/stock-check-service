package com.comcarde.stockcheck.database.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "AUDIT", schema = "AUDIT")
public class AuditRecord {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column(name = "AUDIT_TYPE", nullable = false)
    private AuditType auditType;
    @Column(name = "AUDIT_RECORD", nullable = false)
    private String auditRecord;
    @Column(name = "AUDIT_DATETIME", nullable = false)
    private LocalDateTime auditTime;
}
