package com.comcarde.stockcheck.database.entity;

import com.comcarde.stockcheck.model.OrderType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "STOCK_ITEM", schema = "INVENTORY")
public class StockItem {
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private String itemId;

    @Column(name = "ITEM_NAME", unique = true, nullable = false)
    private String itemName;

    @Column(name = "ITEM_DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "ITEM_STOCK", nullable = false)
    private Integer stockLevel;

    @Transient
    private int oneOffQuantity;

    @Transient
    private OrderType orderType;
}
