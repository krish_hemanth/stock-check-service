package com.comcarde.stockcheck.service;

import com.comcarde.stockcheck.database.entity.Rule;
import com.comcarde.stockcheck.database.repository.RuleRepository;
import com.comcarde.stockcheck.model.OrderType;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.mvel.MVELRule;
import org.mvel2.ParserContext;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class RuleService {

    private RuleRepository ruleRepository;

    public RuleService(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    public Rules getMVelRulesForStockItems() {
        List<Rule> rules = StreamSupport.stream(ruleRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        Collections.sort(rules, new Comparator<Rule>() {
            @Override
            public int compare(Rule r1, Rule r2) {
                return r1.getOrder().compareTo(r2.getOrder());
            }
        });

        Rules mvelRules = new Rules();

        ParserContext parserContext = new ParserContext();
        parserContext.addImport(OrderType.class);

        rules.stream().map(rule -> new MVELRule(parserContext)
                .name(rule.getName())
                .priority(rule.getOrder())
                .description(rule.getDescription())
                .when(rule.getCondition())
                .then(rule.getAction())).forEach(mvelRule -> mvelRules.register(mvelRule));

        return mvelRules;
    }
}
