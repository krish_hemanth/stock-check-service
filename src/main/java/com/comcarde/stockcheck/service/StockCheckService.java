package com.comcarde.stockcheck.service;


import com.comcarde.stockcheck.database.entity.StockItem;
import com.comcarde.stockcheck.database.repository.ItemStockRepository;
import com.comcarde.stockcheck.model.Order;
import com.comcarde.stockcheck.model.OrderType;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class StockCheckService {
    private static final String NO_ITEMS_TO_ORDER = "No items to order";
    private RuleService ruleService;
    private ItemStockRepository itemStockRepository;

    public StockCheckService(RuleService ruleService, ItemStockRepository itemStockRepository) {
        this.ruleService = ruleService;
        this.itemStockRepository = itemStockRepository;
    }

    public StockOderAdviceResponse getStockOrderAdvice() {

        List<StockItem> itemList = StreamSupport.stream(itemStockRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());

        Rules mvelRules = ruleService.getMVelRulesForStockItems();

        itemList.stream().forEach(item -> {
            Facts facts = new Facts();
            facts.put("fact", item);
            RulesEngine rulesEngine = new DefaultRulesEngine();
            rulesEngine.fire(mvelRules, facts);
        });

        return prepareStockAdvice(itemList);
    }

    private StockOderAdviceResponse prepareStockAdvice(List<StockItem> itemList) {
        List<Order> orderList = itemList.stream().filter(item -> item.getOrderType() == OrderType.REORDER)
                .map(item -> Order.builder().itemName(item.getItemName())
                        .description(item.getDescription())
                        .orderType(item.getOrderType().getOrderTypeDescription())
                        .build()).collect(Collectors.toList());

        orderList.addAll(itemList.stream().filter(item -> item.getOneOffQuantity() > 0)
                .map(item -> Order.builder().itemName(item.getItemName())
                        .description(item.getDescription())
                        .orderType(OrderType.ONE_OFF.getOrderTypeDescription())
                        .quantity(item.getOneOffQuantity())
                        .build()).collect(Collectors.toList()));

        return orderList.size() > 0 ? StockOderAdviceResponse.builder()
                .orderList(orderList).build() : StockOderAdviceResponse.builder().message(NO_ITEMS_TO_ORDER).build();
    }
}
