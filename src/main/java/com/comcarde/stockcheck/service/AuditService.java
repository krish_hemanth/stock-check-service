package com.comcarde.stockcheck.service;

import com.comcarde.stockcheck.database.entity.AuditRecord;
import com.comcarde.stockcheck.database.entity.AuditType;
import com.comcarde.stockcheck.database.repository.AuditRepository;
import com.comcarde.stockcheck.exception.ServiceException;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class AuditService {
    private AuditRepository auditRepository;

    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public void saveStockOrderAdvice(StockOderAdviceResponse stockOderAdviceResponse) throws ServiceException {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            AuditRecord auditRecord = AuditRecord.builder()
                    .auditType(AuditType.STOCK_ORDER_ADVICE)
                    .auditRecord(objectMapper.writeValueAsString(stockOderAdviceResponse))
                    .auditTime(LocalDateTime.now()).build();
            auditRepository.save(auditRecord);
        } catch (JsonProcessingException ex) {
            log.error("Error processing json");
            throw new ServiceException("Audit record couldnt be saved");
        }
    }
}
