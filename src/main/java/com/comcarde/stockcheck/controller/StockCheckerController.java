package com.comcarde.stockcheck.controller;

import com.comcarde.stockcheck.aspect.AuditResponse;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import com.comcarde.stockcheck.service.StockCheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@Slf4j
public class StockCheckerController {
    private StockCheckService stockCheckService;
    public StockCheckerController(StockCheckService stockCheckService) {
        this.stockCheckService = stockCheckService;
    }

    @GetMapping(value = "/stock/order/advice", produces = MediaType.APPLICATION_JSON_VALUE)
    @AuditResponse
    public StockOderAdviceResponse getOrderAdvice() {
        log.trace("getOrderAdvice called");
        return stockCheckService.getStockOrderAdvice();
    }
}
