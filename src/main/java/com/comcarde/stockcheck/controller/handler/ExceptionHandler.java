package com.comcarde.stockcheck.controller.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class ExceptionHandler {
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public String handleConflict(Exception ex) {
        log.error("Error occured getting stock advice", ex);

        return "Error preparing stock advice please try again";
    }
}
