package com.comcarde.stockcheck.controller;

import com.comcarde.stockcheck.database.entity.StockItem;
import com.comcarde.stockcheck.database.repository.ItemStockRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class StockItemController {

    private ItemStockRepository itemStockRepository;

    public StockItemController(ItemStockRepository itemStockRepository) {
        this.itemStockRepository = itemStockRepository;
    }

    @PostMapping(value = "/stock/item/clearAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public void clearAllStockItems() {
        itemStockRepository.deleteAll();
    }

    @PostMapping(value = "/stock/item/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void clearAllStockItems(@RequestBody StockItem stockItem) {
        itemStockRepository.save(stockItem);
    }

}
