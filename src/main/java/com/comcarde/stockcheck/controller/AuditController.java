package com.comcarde.stockcheck.controller;

import com.comcarde.stockcheck.database.entity.AuditRecord;
import com.comcarde.stockcheck.database.repository.AuditRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/")
public class AuditController {
    private AuditRepository auditRepository;
    public AuditController(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    @GetMapping(value = "/stock/order/audit", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AuditRecord> getAuditEntries() {
        return StreamSupport.stream(auditRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @PostMapping(value = "/stock/order/audit/clearAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public void clearAllStockItems() {
        auditRepository.deleteAll();
    }
}
