package com.comcarde.stockcheck.aspect;

import com.comcarde.stockcheck.exception.ServiceException;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import com.comcarde.stockcheck.service.AuditService;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditResponseAspect {

    @Autowired
    private AuditService auditService;


    @AfterReturning(
            pointcut = "@annotation(com.comcarde.stockcheck.aspect.AuditResponse)",
            returning = "result")
    public Object afterReturning(final Object result) throws ServiceException {
        auditService.saveStockOrderAdvice(StockOderAdviceResponse.class.cast(result));
        return result;
    }
}
