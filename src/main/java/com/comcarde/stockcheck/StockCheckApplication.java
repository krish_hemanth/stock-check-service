package com.comcarde.stockcheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class StockCheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockCheckApplication.class, args);
    }

}
