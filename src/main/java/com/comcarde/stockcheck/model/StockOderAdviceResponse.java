package com.comcarde.stockcheck.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockOderAdviceResponse {
    private List<Order> orderList;

    private String message;
}
