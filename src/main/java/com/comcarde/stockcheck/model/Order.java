package com.comcarde.stockcheck.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    private String itemName;

    private String description;

    private String orderType;

    private Integer quantity;
}
