package com.comcarde.stockcheck.model;

public enum OrderType {
    REORDER ("Regular reorder"),
    BLOCKED ("Blocked for ordering"),
    ONE_OFF ("Additional one off order");
    String orderTypeDescription;

    OrderType(String orderTypeDescription) {
        this.orderTypeDescription = orderTypeDescription;
    }

    public String getOrderTypeDescription() {
        return orderTypeDescription;
    }
}
