package com.comcarde.stockcheck.database.repository;

import com.comcarde.stockcheck.database.entity.StockItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemStockRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ItemStockRepository itemStockRepository;

    @Before
    public void clearDB () {
        itemStockRepository.deleteAll();
    }

    @Test
    public void testThatEntriesCanBeRetrieved() {
        //  arrange
        StockItem stockItem = createStockItem();
        entityManager.persist(stockItem);
        //  act
        List<StockItem> stockItemsList = StreamSupport.stream(itemStockRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        // assert
        assertThat(stockItemsList).containsOnly(stockItem);
    }

    @Test(expected = PersistenceException.class)
    public void testItemId_null() {
        //  arrange
        StockItem stockItem = createStockItem();
        stockItem.setItemId(null);
        entityManager.persist(stockItem);
    }

    @Test(expected = PersistenceException.class)
    public void testItemName_null() {
        //  arrange
        StockItem stockItem = createStockItem();
        stockItem.setItemName(null);
        entityManager.persist(stockItem);
    }

    @Test(expected = PersistenceException.class)
    public void testItemDescription_null() {
        //  arrange
        StockItem stockItem = createStockItem();
        stockItem.setDescription(null);
        entityManager.persist(stockItem);
    }

    @Test(expected = PersistenceException.class)
    public void testItemStockLevel_null() {
        //  arrange
        StockItem stockItem = createStockItem();
        stockItem.setStockLevel(null);
        entityManager.persist(stockItem);
    }

    private StockItem createStockItem() {
        StockItem stockItem = new StockItem();
        stockItem.setItemId("1");
        stockItem.setItemName("A");
        stockItem.setDescription("Test A");
        stockItem.setStockLevel(10);
        return stockItem;
    }
}