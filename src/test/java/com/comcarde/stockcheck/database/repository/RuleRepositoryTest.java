package com.comcarde.stockcheck.database.repository;

import com.comcarde.stockcheck.database.entity.Rule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RuleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RuleRepository ruleRepository;

    @Before
    public void clearDB () {
        ruleRepository.deleteAll();
    }

    @Test
    public void testThatEntriesCanBeRetrieved() {
        //  arrange
        Rule rule = createRule();
        entityManager.persist(rule);
        //  act
        List<Rule> ruleList = StreamSupport.stream(ruleRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        // assert
        assertThat(ruleList).containsOnly(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testRuleId_null() {
        //  arrange
        Rule rule = createRule();
        rule.setRuleId(null);
        entityManager.persist(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testRuleName_null() {
        //  arrange
        Rule rule = createRule();
        rule.setName(null);
        entityManager.persist(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testRuleCondition_null() {
        //  arrange
        Rule rule = createRule();
        rule.setCondition(null);
        entityManager.persist(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testRuleAction_null() {
        //  arrange
        Rule rule = createRule();
        rule.setAction(null);
        entityManager.persist(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testRuleOrder_null() {
        //  arrange
        Rule rule = createRule();
        rule.setOrder(null);
        entityManager.persist(rule);
    }


    @Test(expected = PersistenceException.class)
    public void testRuleDescription_null() {
        //  arrange
        Rule rule = createRule();
        rule.setDescription(null);
        entityManager.persist(rule);
    }

    @Test(expected = PersistenceException.class)
    public void testSameRuleName() {
        //  arrange
        Rule rule = createRule();
        entityManager.persist(rule);

        Rule rule1 = secondRule();
        rule1.setName("A");
        entityManager.persistAndFlush(rule1);

    }

    @Test(expected = PersistenceException.class)
    public void testSameRuleOrder() {
        //  arrange
        Rule rule = createRule();
        entityManager.persist(rule);

        Rule rule1 = secondRule();
        rule1.setOrder(10);
        entityManager.persistAndFlush(rule1);

    }

    private Rule createRule() {
        Rule rule = new Rule();
        rule.setRuleId("1");
        rule.setName("A");
        rule.setCondition("1==1");
        rule.setAction("return true");
        rule.setOrder(10);
        rule.setDescription("rule a");
        return rule;
    }

    private Rule secondRule() {
        Rule rule = new Rule();
        rule.setRuleId("2");
        rule.setName("B");
        rule.setCondition("1==1");
        rule.setAction("return true");
        rule.setOrder(20);
        rule.setDescription("rule a");
        return rule;
    }

}