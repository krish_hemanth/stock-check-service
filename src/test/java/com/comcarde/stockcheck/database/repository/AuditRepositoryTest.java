package com.comcarde.stockcheck.database.repository;


import com.comcarde.stockcheck.database.entity.AuditRecord;
import com.comcarde.stockcheck.database.entity.AuditType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuditRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AuditRepository auditRepository;

    @Before
    public void clearDB () {
        auditRepository.deleteAll();
    }

    @Test
    public void testThatEntriesCanBeRetrieved() {
        //  arrange
        AuditRecord auditRecord = createAuditRecord();
        entityManager.persist(auditRecord);
        //  act
        List<AuditRecord> recordsFromDB = StreamSupport.stream(auditRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        //  assert
        assertThat(recordsFromDB).containsOnly(auditRecord);
        assertNotNull(recordsFromDB.get(0).getId());
    }

    @Test(expected = PersistenceException.class)
    public void testAuditType_null() {
        //  arrange
        AuditRecord auditRecord = createAuditRecord();
        auditRecord.setAuditType(null);
        entityManager.persist(auditRecord);
    }

    @Test(expected = PersistenceException.class)
    public void testAuditRecord_null() {
        //  arrange
        AuditRecord auditRecord = createAuditRecord();
        auditRecord.setAuditRecord(null);
        entityManager.persist(auditRecord);
    }

    @Test(expected = PersistenceException.class)
    public void testAuditTime_null() {
        //  arrange
        AuditRecord auditRecord = createAuditRecord();
        auditRecord.setAuditTime(null);
        entityManager.persist(auditRecord);
    }

    private AuditRecord createAuditRecord() {
        AuditRecord auditRecord = new AuditRecord();
        auditRecord.setAuditRecord("{\"orderList\":[{\"itemName\":\"D\",\"description\":\"Item D\",\"orderType\":\"Regular reorder\"},{\"itemName\":\"E\",\"description\":\"Item E\",\"orderType\":\"Regular reorder\"},{\"itemName\":\"D\",\"description\":\"Item D\",\"orderType\":\"Additional one off order\",\"quantity\":15}]}");
        auditRecord.setAuditTime(LocalDateTime.of(2021,2,1,5,0));
        auditRecord.setAuditType(AuditType.STOCK_ORDER_ADVICE);
        return auditRecord;
    }
}