package com.comcarde.stockcheck.service;

import com.comcarde.stockcheck.database.entity.Rule;
import com.comcarde.stockcheck.database.repository.RuleRepository;
import org.jeasy.rules.api.Rules;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RuleServiceTest {

    @Mock
    private RuleRepository ruleRepository;

    @InjectMocks
    private RuleService ruleService;


    @Test
    public void testgetRules() {
        when(ruleRepository.findAll()).thenReturn(getRules());
        Rules mvelRules = ruleService.getMVelRulesForStockItems();
        assertEquals(mvelRules.size(), 2);

        Optional<org.jeasy.rules.api.Rule> ruleA = StreamSupport.stream(mvelRules.spliterator(), false).filter(mvelRule -> mvelRule.getName().contains("Rule A")).findFirst();
        assertTrue(ruleA.isPresent());

        Optional<org.jeasy.rules.api.Rule> ruleB = StreamSupport.stream(mvelRules.spliterator(), false).filter(mvelRule -> mvelRule.getName().contains("Rule A")).findFirst();
        assertTrue(ruleB.isPresent());
    }

    private List<Rule> getRules() {
        Rule rule1 = Rule.builder()
                .name("Rule A")
                .condition("1==1")
                .action("return true")
                .order(10).build();

        Rule rule2 = Rule.builder()
                .name("Rule B")
                .condition("2==2")
                .action("return true")
                .order(20).build();
        return Arrays.asList(rule2,rule1);
    }


}