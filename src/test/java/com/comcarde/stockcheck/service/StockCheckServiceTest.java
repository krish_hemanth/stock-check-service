package com.comcarde.stockcheck.service;


import com.comcarde.stockcheck.database.entity.StockItem;
import com.comcarde.stockcheck.database.repository.ItemStockRepository;
import com.comcarde.stockcheck.model.OrderType;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.mvel.MVELRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mvel2.ParserContext;

import javax.persistence.NoResultException;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StockCheckServiceTest {

    @Mock
    private RuleService ruleService;

    @Mock
    private ItemStockRepository itemStockRepository;

    @InjectMocks
    private StockCheckService stockCheckService;


    @Test(expected = NoResultException.class)
    public void testgetStockOrderAdvice_itemRepository_throwsException() {
        when(itemStockRepository.findAll()).thenThrow(new NoResultException());
        stockCheckService.getStockOrderAdvice();
    }

    @Test
    public void testGetStockOrderAdvice_onlyReorderItems() {
        when(itemStockRepository.findAll()).thenReturn(getStockItems());
        when(ruleService.getMVelRulesForStockItems()).thenReturn(getReorderMvelRules());
        StockOderAdviceResponse response = stockCheckService.getStockOrderAdvice();
        assertEquals(response.getOrderList().size(), 1);
        assertTrue(response.getOrderList().get(0).getItemName().equals("Item B"));
        assertTrue(response.getOrderList().get(0).getOrderType().equals(OrderType.REORDER.getOrderTypeDescription()));
    }


    @Test
    public void testGetStockOrderAdvice_onlyOneoffOrderItems() {
        when(itemStockRepository.findAll()).thenReturn(getStockItems());
        when(ruleService.getMVelRulesForStockItems()).thenReturn(getOneoffOrderMvelRules());
        StockOderAdviceResponse response = stockCheckService.getStockOrderAdvice();
        assertEquals(response.getOrderList().size(), 1);
        assertTrue(response.getOrderList().get(0).getItemName().equals("Item A"));
        assertTrue(response.getOrderList().get(0).getOrderType().equals(OrderType.ONE_OFF.getOrderTypeDescription()));
    }

    @Test
    public void testGetStockOrderAdvice_OneoffAndReOrderItems() {
        when(itemStockRepository.findAll()).thenReturn(getStockItems());
        when(ruleService.getMVelRulesForStockItems()).thenReturn(getReorderAndOneoffMvelRules());
        StockOderAdviceResponse response = stockCheckService.getStockOrderAdvice();
        assertEquals(response.getOrderList().size(), 2);
        assertTrue(response.getOrderList().get(0).getItemName().equals("Item B"));
        assertTrue(response.getOrderList().get(0).getOrderType().equals(OrderType.REORDER.getOrderTypeDescription()));
        assertTrue(response.getOrderList().get(1).getItemName().equals("Item A"));
        assertTrue(response.getOrderList().get(1).getOrderType().equals(OrderType.ONE_OFF.getOrderTypeDescription()));
    }


    private List<StockItem> getStockItems() {
        StockItem itemA = StockItem.builder()
                .itemId("1")
                .itemName("Item A")
                .stockLevel(5).build();

        StockItem itemB = StockItem.builder()
                .itemId("2")
                .itemName("Item B")
                .stockLevel(4).build();

        return Arrays.asList(itemA, itemB);
    }
    private Rules getReorderAndOneoffMvelRules() {
        ParserContext parserContext = new ParserContext();
        parserContext.addImport(OrderType.class);

        Rules rules = new Rules();
        rules.register(getReorderRule(parserContext));
        rules.register(getOneOffRule(parserContext));

        return rules;
    }


    private Rules getReorderMvelRules() {
        ParserContext parserContext = new ParserContext();
        parserContext.addImport(OrderType.class);

        Rules rules = new Rules();
        rules.register(getReorderRule(parserContext));

        return rules;
    }


    private Rules getOneoffOrderMvelRules() {
        ParserContext parserContext = new ParserContext();
        parserContext.addImport(OrderType.class);
        MVELRule mvelRule = getOneOffRule(parserContext);

        Rules rules = new Rules();
        rules.register(mvelRule);

        return rules;
    }

    private MVELRule getReorderRule(ParserContext parserContext) {
        MVELRule mvelRule = new MVELRule(parserContext);
        mvelRule.priority(10);
        mvelRule.when("fact.stockLevel <= 4");
        mvelRule.then("fact.orderType=OrderType.REORDER");
        return mvelRule;
    }

    private MVELRule getOneOffRule(ParserContext parserContext) {
        MVELRule mvelRule = new MVELRule(parserContext);
        mvelRule.priority(20);
        mvelRule.when("fact.itemName.equals(\"Item A\")");
        mvelRule.then("fact.oneOffQuantity=15");
        return mvelRule;
    }
}