package com.comcarde.stockcheck.aspect;

import com.comcarde.stockcheck.controller.StockCheckerController;
import com.comcarde.stockcheck.exception.ServiceException;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import com.comcarde.stockcheck.service.AuditService;
import com.comcarde.stockcheck.service.StockCheckService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;



@RunWith(MockitoJUnitRunner.class)
public class AuditResponseAspectTest {

    @Mock
    private AuditService auditService;

    @Mock
    private StockCheckService stockCheckService;

    @Mock
    private StockOderAdviceResponse mockStockOderAdviceResponse;

    @InjectMocks
    private AuditResponseAspect aspect;

    @Test
    public void testAuditServiceCalled() throws ServiceException {
        StockCheckerController target = new StockCheckerController(stockCheckService);
        AspectJProxyFactory factory = new AspectJProxyFactory(target);
        factory.addAspect(aspect);
        StockCheckerController proxy = factory.getProxy();


        Mockito.when(stockCheckService.getStockOrderAdvice()).thenReturn(mockStockOderAdviceResponse);
        proxy.getOrderAdvice();

        Mockito.verify(auditService).saveStockOrderAdvice(mockStockOderAdviceResponse);
    }

    @Test
    public void testAuditServiceNotCalled() throws ServiceException {
        StockCheckerController target = new StockCheckerController(stockCheckService);
        AspectJProxyFactory factory = new AspectJProxyFactory(target);
        factory.addAspect(aspect);
        StockCheckerController proxy = factory.getProxy();


        Mockito.when(stockCheckService.getStockOrderAdvice()).thenThrow(new RuntimeException("error preparing stock advice"));
        try {
            proxy.getOrderAdvice();
        } catch (RuntimeException ex) {}

        Mockito.verify(auditService, Mockito.never()).saveStockOrderAdvice(mockStockOderAdviceResponse);
    }
}