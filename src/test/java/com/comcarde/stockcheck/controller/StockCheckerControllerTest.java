package com.comcarde.stockcheck.controller;

import com.comcarde.stockcheck.model.Order;
import com.comcarde.stockcheck.model.OrderType;
import com.comcarde.stockcheck.model.StockOderAdviceResponse;
import com.comcarde.stockcheck.service.StockCheckService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.PersistenceException;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class StockCheckerControllerTest {

    @Mock
    private StockCheckService stockCheckService;

    @InjectMocks
    private StockCheckerController stockCheckerController;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(stockCheckerController).build();
    }

    @Test
    public void testGetStockAdvice_orderList() throws Exception {

        Order order = Order.builder().itemName("A")
                .orderType(OrderType.REORDER.getOrderTypeDescription())
                .description("Item A")
                .build();

        StockOderAdviceResponse response = StockOderAdviceResponse.builder()
                .orderList(Arrays.asList(order))
                .build();
        //given
        when(stockCheckService.getStockOrderAdvice()).thenReturn(response);

        //when
        MvcResult result = this.mockMvc.perform(get("/stock/order/advice")
                .contentType(MediaType.APPLICATION_JSON))

                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentType()).contains("application/json");
        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"orderList\":[{\"itemName\":\"A\",\"description\":\"Item A\",\"orderType\":\"Regular reorder\"}]}");
    }

    @Test
    public void testGetStockAdvice_message() throws Exception {

        StockOderAdviceResponse response = StockOderAdviceResponse.builder()
                .message("No items to order").build();
        //given
        when(stockCheckService.getStockOrderAdvice()).thenReturn(response);

        //when
        MvcResult result = this.mockMvc.perform(get("/stock/order/advice")
                .contentType(MediaType.APPLICATION_JSON))

                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentType()).contains("application/json");
        assertThat(result.getResponse().getContentAsString()).isEqualTo("{\"message\":\"No items to order\"}");
    }
}