Feature: get stock order advice

  Scenario: Get stock order advice when no items to reorders
    Given Prepare for inventory
    When Add Item {"itemId":"1", "itemName": "A", "description": "Item A", "stockLevel": 5}
    When I try to get stock order advice
    Then the response will be {"message":"No items to order"}
    And the Audit table has an entry


  Scenario: Get stock order advice
    Given Prepare for inventory
    When Add Item {"itemId":"1", "itemName": "A", "description": "Item A", "stockLevel": 5}
    When Add Item {"itemId":"2", "itemName": "B", "description": "Item B", "stockLevel": 8}
    When Add Item {"itemId":"3", "itemName": "C", "description": "Item C", "stockLevel": 2}
    When Add Item {"itemId":"4", "itemName": "D", "description": "Item D", "stockLevel": 1}
    When Add Item {"itemId":"5", "itemName": "E", "description": "Item E", "stockLevel": 0}

    When I try to get stock order advice
    Then the response will be {"orderList":[{"itemName":"D","description":"Item D","orderType":"Regular reorder"},{"itemName":"E","description":"Item E","orderType":"Regular reorder"},{"itemName":"D","description":"Item D","orderType":"Additional one off order","quantity":15}]}
    And the Audit table has an entry

