package com.comcarde.stockcheck.bdd;

import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

@CucumberContextConfiguration
@SpringBootTest
public abstract class AcceptanceTestStepDefs {
    protected RestTemplate restTemplate = new RestTemplate();

    protected final String DEFAULT_URL = "http://localhost:8080/";
}
