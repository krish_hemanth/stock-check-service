package com.comcarde.stockcheck.bdd;

import com.comcarde.stockcheck.database.entity.AuditRecord;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

public class StockOrderAdviceServiceStepDefs extends AcceptanceTestStepDefs {
    private String response ;
    private final String STOCK_ORDER_ADVICE_URL = DEFAULT_URL + "/stock/order/advice";
    private final String AUDIT_URL = DEFAULT_URL + "/stock/order/audit";
    private final String CLEAR_INVENTORY_URL = DEFAULT_URL + "/stock/item/clearAll";
    private final String ADD_ITEM_URL = DEFAULT_URL + "/stock/item/add";
    private final String CLEAR_AUDIT_URL = DEFAULT_URL + "/stock/order/audit/clearAll";

    @Given("^Prepare for inventory$")
    public void prepareForInventory() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        restTemplate.exchange(CLEAR_INVENTORY_URL, HttpMethod.POST, entity, String.class);

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<>(headers);
        restTemplate.exchange(CLEAR_AUDIT_URL, HttpMethod.POST, entity, String.class);
    }

    @When("^Add Item (.*)$")
    public void prepareForInventory(String stockItem) throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(stockItem, headers);
        restTemplate.exchange(ADD_ITEM_URL, HttpMethod.POST, entity, String.class);
    }

    @When("^I try to get stock order advice$")
    public void getStockOrderAdvice() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        response = restTemplate.exchange(STOCK_ORDER_ADVICE_URL, HttpMethod.GET, entity, String.class).getBody();
    }

    @Then("^the response will be (.*)")
    public void checkResponse(String expectedResponse) {
        assertThat(response).isEqualTo(expectedResponse);
    }

    @And("the Audit table has an entry")
    public void auditTableEntries() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        String auditResponse = restTemplate.exchange(AUDIT_URL, HttpMethod.GET, entity, String.class).getBody();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        List<AuditRecord> auditRecords = objectMapper.readValue(auditResponse, new TypeReference<List<AuditRecord>>(){});
        assertTrue(auditRecords.get(0).getAuditRecord().equals(response));
    }
}

