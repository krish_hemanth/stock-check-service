package com.comcarde.stockcheck.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features/",
        glue = "com.comcarde.stockcheck.bdd")
public class CucumberAcceptanceTest {
}
